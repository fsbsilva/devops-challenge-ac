AvenueCode Configuration Management Challenge
------

Your task is to manage the state of a node using configuration management.

We should be able to use your code to manage the state of any node but to better automate this assesment, we will manage the state of `localhost`.

Your configuration should be able to:

1. Manage a web server (tomcat, jetty, jboss, your choice) installed and started at port 8080
1. Have a WAR file deployed to that web server

The war file can be reached at `./assets/java-chef-test.war`.

The test page for the WAR file is:

http://localhost:8080/java-chef-test/chef/ping

Tests should show that this page is reachable and is specific to the content the test page sends back.


You can pick the configuration management tool from the below list:

1. Puppet
1. Chef
1. Ansible

Keep in mind that even thought we have an automated pipeline to execute your code, we will read it and we might ask questions about it during the technical interview (if you pass in this assessment), make sure you know how to explain your approach in this challenge.

What we will review in your code:

1. Code design
1. Best practices

Unit and integration tests are not mandatory, but if you have time, please write tests :)


How we will Evaluate
--------------------

If you already read `candidate-preferences.yml` you have probably noticed that we will run your challenge in a virtual machine inside AWS. We have provided two AMIs per cfg tool, one with ubuntu:16.04 and another with centos7.

You can decide if you want to solve this challenge targeting ubuntu or centos.

We expect to clone your repo inside `/home/avenuecode`, run `deps_command` to do any kind of preparation work, such as installing dependencies and then provision the machine using `provision_command`.

**The absolute path** will be then: `/home/avenuecode/devops-challenge-ac002/cfg-management`

We think that you should develop using `vagrant` or `docker` or any tool that makes you more productive, but we strongly recommend you to perform a final test using the AMI we've provided.

The test is fairly simple, you should avoid external dependencies.

Checklist
------------

1. You have filled `candidate-preferences.yml`

1. You have tested your code using the AvenueCode AMI and it works fine

1. We can run all steps below from this folder, not from the root repository directory.

1. Using the information provided in your `candidate-preferences.yml`

    3.1 If we execute `deps_command` we have everything set to run your `provision_command`

    3.3 If we run `provision_command` we can reach `http://localhost:8080/java-chef-test/chef/ping` and it returns `Hello Chef deployed war`.



Thank you,
The AvenueCode Recruiting Team
